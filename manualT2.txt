﻿Manual de Execução - Simulador Universal de Máquinas de Turing


Para executar esse programa, você deve seguir os seguintes passos:


* Windows
   * Aperte as teclas Windows+R
   * Quando a janela "Executar" abrir, digite "cmd" e aperte enter
   * Copie o caminho da pasta onde os arquivos do trabalho estão
   * No terminal, digite cd e copie o caminho da pasta
      * Exemplo: cd C:\Users\Iaram\Downloads\EntregaTrabalho1
   * Agora, digite "simulador.exe" para rodar o programa
EXECUÇÃO DO PROGRAMA
   * Digitar as entradas desejadas:
      * Número de Estados
      * Quantidade de símbolos terminais, seguidos pelos mesmos símbolos separados por espaço simples
      * Quantidade de símbolos estendidos de fita, seguidos pelos mesmos símbolos separados por espaço simples
      * Número de transições
      * Transição na forma q x q’ y D onde q,q’ pertencem a Q, x,y pertencem a E’ e D pertence a {R,L,S} - sendo B o símbolo branco e a cadeia vazia como “-” (1 por linha)
      * Quantidade de Cadeias de Entrada 
      * Cadeia de Entrada (1 por linha)
Ou, você pode também fazer a execução por uma IDE de sua preferência, abrindo o arquivo “simulador_automatos.c”, compilando e executando.


* Linux
   * Abrir o terminal
   * Acessar a pasta
   * Digitar “make all” e compilar o programa
   * Digitar “make run” e executar o programa
   * Digitar as entradas desejadas:
      * Número de Estados
      * Quantidade de símbolos terminais, seguidos pelos mesmos símbolos separados por espaço simples
      * Quantidade de símbolos estendidos de fita, seguidos pelos mesmos símbolos separados por espaço simples
      * Número de transições
      * Transição na forma q x q’ y D onde q,q’ pertencem a Q, x,y pertencem a E’ e D pertence a {R,L,S} - sendo B o símbolo branco e a cadeia vazia como “-” (1 por linha)
      * Quantidade de Cadeias de Entrada 
      * Cadeia de Entrada (1 por linha)